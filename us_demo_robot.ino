/*
 * ESP8266 NodeMCU LED Control over WiFi Demo
 *
 * https://circuits4you.com
 */
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
 
//ESP Web Server Library to host a web page
#include <ESP8266WebServer.h>
#include <WS2812FX.h>


#ifndef UNIT_TEST
#include <Arduino.h>
#endif
#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>
const uint16_t kRecvPin = 14;
 
//---------------------------------------------------------------
//Our HTML webpage contents in program memory
const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
<body>
<center>
<h1>WiFi color control:  1</h1><br>
Ciclk to turn <a href="red" target="myIframe">Red</a><br>
Ciclk to turn <a href="rose" target="myIframe">Rose</a><br>
Ciclk to turn <a href="orange" target="myIframe">Orange</a><br>
Ciclk to turn <a href="butter" target="myIframe">Butter</a><br>
Ciclk to turn <a href="pine" target="myIframe">Pine</a><br>
Ciclk to turn <a href="green" target="myIframe">Green</a><br>
Ciclk to turn <a href="arctic" target="myIframe">Arctic</a><br>
Ciclk to turn <a href="sky" target="myIframe">Sky</a><br>
Ciclk to turn <a href="cobalt" target="myIframe">Cobalt</a><br>
Ciclk to turn <a href="berry" target="myIframe">Berry</a><br>
Ciclk to turn <a href="lapis" target="myIframe">Lapis</a><br>
Ciclk to turn <a href="violet" target="myIframe">Violet</a><br>
Ciclk to turn <a href="orchid" target="myIframe">Orchid</a><br>
Ciclk to turn <a href="pink" target="myIframe">Pink</a><br>
Ciclk to turn <a href="white" target="myIframe">White</a><br>
LED State:<iframe name="myIframe" width="100" height="25" frameBorder="0"><br>
<hr>
<a href="https://circuits4you.com">circuits4you.com</a>
</center>
 
</body>
</html>
)=====";
//---------------------------------------------------------------
//On board LED Connected to GPIO2
#define LED 2


#define LED_COUNT 2
#define LED_PIN 2

#define red 255,0,0
#define rose 255,20,0
#define orange 255,50,0
#define butter 255,120,0
#define yellow 255,255,0
#define pine 2,51,3
#define green 2,255,2
#define arctic 20,215,255
#define sky 20,32,255
#define cobalt 9,13,84
#define berry 9,11,51
#define lapis 20,20,255
#define violet 73,0,102
#define orchid 90,16,119
#define pink 232,4,197
#define white 255,255,255


WS2812FX ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);


IRrecv irrecv(kRecvPin);

decode_results results;
bool handleClientBool;
  
 
//SSID and Password of your WiFi router
const char* ssid = "iPhone";
const char* password = "cyberpass";
 
//Declare a global object variable from the ESP8266WebServer class.
ESP8266WebServer server(80); //Server on port 80
 
//===============================================================
// This routine is executed when you open its IP in browser
//===============================================================
void handleRoot() {
 Serial.println("You called root page");
 String s = MAIN_page; //Read HTML contents
 server.send(200, "text/html", s); //Send web page
}
 
void handleRed() { 
 Serial.println("red page");
 ws2812fx.setColor(red);
 server.send(200, "text/html", "Red"); //Send ADC value only to client ajax request
}
 
void handleRose() { 
 Serial.println("rose page");
 ws2812fx.setColor(rose);
 server.send(200, "text/html", "Rose");
} 

void handleOrange() { 
 Serial.println("orange page");
 ws2812fx.setColor(orange);
 server.send(200, "text/html", "Orange");
}

void handleButter() { 
 Serial.println("butter page");
 ws2812fx.setColor(butter);
 server.send(200, "text/html", "Butter"); 
}

void handleYellow() { 
 Serial.println("yellow page");
 ws2812fx.setColor(yellow);
 server.send(200, "text/html", "Yellow");
}

void handlePine() { 
 Serial.println("pine page");
 ws2812fx.setColor(pine);
 server.send(200, "text/html", "Pine");
}

void handleGreen() { 
 Serial.println("green page");
 ws2812fx.setColor(green);
 server.send(200, "text/html", "Green");
}

void handleArctic() { 
 Serial.println("arctic page");
 ws2812fx.setColor(arctic);
 server.send(200, "text/html", "Arctic");
}

void handleSky() { 
 Serial.println("sky page");
 ws2812fx.setColor(sky);
 server.send(200, "text/html", "Sky");
}

void handleCobalt() { 
 Serial.println("cobalt page");
 ws2812fx.setColor(cobalt);
 server.send(200, "text/html", "Cobalt");
}

void handleBerry() { 
 Serial.println("berry page");
 ws2812fx.setColor(berry);
 server.send(200, "text/html", "Berry");
}

void handleLapis() { 
 Serial.println("lapis page");
 ws2812fx.setColor(lapis);
 server.send(200, "text/html", "Lapis");
}

void handleViolet() { 
 Serial.println("violet page");
 ws2812fx.setColor(violet);
 server.send(200, "text/html", "Violet");
}

void handleOrchid() { 
 Serial.println("orchid page");
 ws2812fx.setColor(orchid);
 server.send(200, "text/html", "Orchid");
}

void handlePink() { 
 Serial.println("pink page");
 ws2812fx.setColor(pink);
 server.send(200, "text/html", "Pink");
}

void handleWhite() { 
 Serial.println("white page");
 ws2812fx.setColor(white);
 server.send(200, "text/html", "White");
}
//==============================================================
//                  SETUP
//==============================================================
void setup(void){
  Serial.begin(115200);
  
  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");
 
  //Onboard LED port Direction output
  pinMode(LED,OUTPUT); 
  //Power on LED state off
  digitalWrite(LED,HIGH);
  
  // Wait for connection
  int waitTime = millis() + 10000; 
  while ( (WiFi.status() != WL_CONNECTED) and (millis() < waitTime) ) { 
  //while ( (WiFi.status() != WL_CONNECTED) ) {
    delay(500);
    Serial.print(".");
  }

  if (WiFi.status() == WL_CONNECTED){
      handleClientBool = true;
    }
  else {
      handleClientBool = false;
    }
  
 
  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
 
  server.on("/", handleRoot);      //Which routine to handle at root location. This is display page
  server.on("/red", handleRed); //as Per  <a href="ledOn">, Subroutine to be called
  server.on("/rose", handleRose);
  server.on("/orange", handleOrange);
  server.on("/butter", handleButter);
  server.on("/yellow", handleYellow);
  server.on("/pine", handlePine);
  server.on("/green", handleGreen);
  server.on("/arctic", handleArctic);
  server.on("/sky", handleSky);
  server.on("/cobalt", handleCobalt);
  server.on("/berry", handleBerry);
  server.on("/lapis", handleLapis);
  server.on("/violet", handleViolet);
  server.on("/orchid", handleOrchid);
  server.on("/pink", handlePink);
  server.on("/white", handleWhite);
 
 
  server.begin();                  //Start server
  Serial.println("HTTP server started");


  irrecv.enableIRIn();  // Start the receiver
  while (!Serial)  // Wait for the serial connection to be establised.
    delay(50);
  Serial.println();
  Serial.print("IRrecvDemo is now running and waiting for IR message on Pin ");
  Serial.println(kRecvPin);

  
  ws2812fx.init();
  ws2812fx.setBrightness(100);
  ws2812fx.setColor(0, 0, 0);
  ws2812fx.start();
}
//==============================================================
//                     LOOP
//==============================================================
void loop(void){
  if (handleClientBool) {
    server.handleClient();
  }          
  ws2812fx.service();
    if (irrecv.decode(&results)) {
    // print() & println() can't handle printing long longs. (uint64_t)
    serialPrintUint64(results.value, HEX);

    switch (results.value) {
      case 0xFF906F:
        ws2812fx.setColor(red);//1
        break;
      case 0xFFB04F:
        ws2812fx.setColor(rose);//2
        break;
      case 0xFFA857:
        ws2812fx.setColor(orange);//3
        break;
      case 0xFF9867:
        ws2812fx.setColor(butter);//4
        break;
      case 0xFF8877:
        ws2812fx.setColor(yellow);//5
        break;
      case 0xFF10EF:
        ws2812fx.setColor(pine);//6
        break;
      case 0xFF30CF:
        ws2812fx.setColor(green);//7
        break;
      case 0xFF28D7:
        ws2812fx.setColor(arctic);//8
        break;
      case 0xFF18E7:
        ws2812fx.setColor(sky);//9
        break;
      case 0xFF08F7:
        ws2812fx.setColor(cobalt);//10
        break;
      case 0xFF50AF:
        ws2812fx.setColor(berry);//11
        break;
      case 0xFF708F:
        ws2812fx.setColor(lapis);//12
        break;
      case 0xFF6897:
        ws2812fx.setColor(violet);//13
        break;
      case 0xFF58A7:
        ws2812fx.setColor(orchid);//14
        break;
      case 0xFF48B7:
        ws2812fx.setColor(pink);//15
        break;
      case 0xFFD02F:
        ws2812fx.setColor(white);//16
        break;
    }
    
    Serial.println("");
    irrecv.resume();  // Receive the next value
  }
  delay(100);  
}
